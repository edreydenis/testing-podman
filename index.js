const app = require('express')();

app.get('/', (req, res) => {
    res.send("Hello, World from a Podman Container!");
})

app.listen(8080, () =>{
    console.log("Server running on Port 8080.");
})