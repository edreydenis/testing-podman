# hello

this is just a test to see how podman really works with a simple nodejs _"app"_

to use it:

run `podman build .`

then `podman images`

check the container ID, and then `podman run -p 8080:8080 <image-name>`

any other info you can find at: https://fedoramagazine.org/getting-started-with-podman-in-fedora/
